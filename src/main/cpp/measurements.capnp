@0xe9e172ef0f0049f6;

struct CapnpRequest {
    records @0 :List(CapnpRecords);
}

struct CapnpRecords {
    info @0 :CapnpInfo;
    records @1 :CapnpRecordsMap;
}

struct CapnpInfo {
    id @0 :UInt32;
    timestamp @1 :UInt64;
    measurerName @2 :Text;
}

struct CapnpRecordsMap {
  entries @0 :List(Entry);
  struct Entry {
    key @0 :UInt32;
    value @1 :List(Float64);
  }
}



struct CapnpResponse {
    pd @0 :List(CapnpProcessedData);
}

struct CapnpProcessedData {
    avgDownload @0 :Float64;
    avgUpload @1 :Float64;
    avgPing @2 :Float64;
    info @3 :CapnpInfo;
}