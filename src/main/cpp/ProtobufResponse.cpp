//
// Created by anton on 4/26/21.
//

#include "ProtobufResponse.h"


Averages *calculateAverage(const Records &records) {
    double avg_upload = 0., avg_download = 0., avg_ping = 0.;

    auto records_upload = records.records().at(1).values();
    auto records_download = records.records().at(0).values();
    auto records_ping = records.records().at(2).values();

    for (int i = 0; i < records_upload.size(); i++) {
        avg_upload = avg_upload + records_upload[i];
    }
    avg_upload /= records_upload.size();

    for (int i = 0; i < records_download.size(); i++) {
        avg_download += records_download[i];
    }
    avg_download /= records_download.size();

    for (int i = 0; i < records_ping.size(); i++) {
        avg_ping += records_ping[i];
    }
    avg_ping /= records_ping.size();

    Averages *averages = new Averages;
    averages->set_upload(avg_upload);
    averages->set_download(avg_download);
    averages->set_ping(avg_ping);

    return averages;
}

void processData(ProcessedData *pd, const Records &records) {
    Info *info = new Info;
    info->set_id(records.info().id());
    info->set_timestamp(records.info().timestamp());
    info->set_measurername(records.info().measurername());

    pd->set_allocated_info(info);
    pd->set_allocated_avg(calculateAverage(records));

}