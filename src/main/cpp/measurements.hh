/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MEASUREMENTS_HH_3872195893__H_
#define MEASUREMENTS_HH_3872195893__H_

#include <sstream>
#include "boost/any.hpp"
#include "avro-cpp-1.10.2/api/Specific.hh"
#include "avro-cpp-1.10.2/api/Encoder.hh"
#include "avro-cpp-1.10.2/api/Decoder.hh"

namespace c {
struct AvroInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AvroInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct AvroRecords {
    AvroInfo info;
    std::map<std::string, std::vector<double > > records;
    AvroRecords() :
        info(AvroInfo()),
        records(std::map<std::string, std::vector<double > >())
        { }
};

struct AvroRequest {
    std::vector<AvroRecords > records;
    AvroRequest() :
        records(std::vector<AvroRecords >())
        { }
};

struct AvroProcessedData {
    std::map<std::string, double > avg;
    AvroInfo info;
    AvroProcessedData() :
        avg(std::map<std::string, double >()),
        info(AvroInfo())
        { }
};

struct AvroResponse {
    std::vector<AvroProcessedData > pd;
    AvroResponse() :
        pd(std::vector<AvroProcessedData >())
        { }
};

struct measurements_avsc_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AvroInfo get_AvroInfo() const;
    void set_AvroInfo(const AvroInfo& v);
    AvroRecords get_AvroRecords() const;
    void set_AvroRecords(const AvroRecords& v);
    AvroRequest get_AvroRequest() const;
    void set_AvroRequest(const AvroRequest& v);
    AvroProcessedData get_AvroProcessedData() const;
    void set_AvroProcessedData(const AvroProcessedData& v);
    AvroResponse get_AvroResponse() const;
    void set_AvroResponse(const AvroResponse& v);
    measurements_avsc_Union__0__();
};

inline
AvroInfo measurements_avsc_Union__0__::get_AvroInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AvroInfo >(value_);
}

inline
void measurements_avsc_Union__0__::set_AvroInfo(const AvroInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
AvroRecords measurements_avsc_Union__0__::get_AvroRecords() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AvroRecords >(value_);
}

inline
void measurements_avsc_Union__0__::set_AvroRecords(const AvroRecords& v) {
    idx_ = 1;
    value_ = v;
}

inline
AvroRequest measurements_avsc_Union__0__::get_AvroRequest() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AvroRequest >(value_);
}

inline
void measurements_avsc_Union__0__::set_AvroRequest(const AvroRequest& v) {
    idx_ = 2;
    value_ = v;
}

inline
AvroProcessedData measurements_avsc_Union__0__::get_AvroProcessedData() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AvroProcessedData >(value_);
}

inline
void measurements_avsc_Union__0__::set_AvroProcessedData(const AvroProcessedData& v) {
    idx_ = 3;
    value_ = v;
}

inline
AvroResponse measurements_avsc_Union__0__::get_AvroResponse() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AvroResponse >(value_);
}

inline
void measurements_avsc_Union__0__::set_AvroResponse(const AvroResponse& v) {
    idx_ = 4;
    value_ = v;
}

inline measurements_avsc_Union__0__::measurements_avsc_Union__0__() : idx_(0), value_(AvroInfo()) { }
}
namespace avro {
template<> struct codec_traits<c::AvroInfo> {
    static void encode(Encoder& e, const c::AvroInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, c::AvroInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<c::AvroRecords> {
    static void encode(Encoder& e, const c::AvroRecords& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, c::AvroRecords& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<c::AvroRequest> {
    static void encode(Encoder& e, const c::AvroRequest& v) {
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, c::AvroRequest& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<c::AvroProcessedData> {
    static void encode(Encoder& e, const c::AvroProcessedData& v) {
        avro::encode(e, v.avg);
        avro::encode(e, v.info);
    }
    static void decode(Decoder& d, c::AvroProcessedData& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.avg);
                    break;
                case 1:
                    avro::decode(d, v.info);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.avg);
            avro::decode(d, v.info);
        }
    }
};

template<> struct codec_traits<c::AvroResponse> {
    static void encode(Encoder& e, const c::AvroResponse& v) {
        avro::encode(e, v.pd);
    }
    static void decode(Decoder& d, c::AvroResponse& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.pd);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.pd);
        }
    }
};

template<> struct codec_traits<c::measurements_avsc_Union__0__> {
    static void encode(Encoder& e, c::measurements_avsc_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AvroInfo());
            break;
        case 1:
            avro::encode(e, v.get_AvroRecords());
            break;
        case 2:
            avro::encode(e, v.get_AvroRequest());
            break;
        case 3:
            avro::encode(e, v.get_AvroProcessedData());
            break;
        case 4:
            avro::encode(e, v.get_AvroResponse());
            break;
        }
    }
    static void decode(Decoder& d, c::measurements_avsc_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 5) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                c::AvroInfo vv;
                avro::decode(d, vv);
                v.set_AvroInfo(vv);
            }
            break;
        case 1:
            {
                c::AvroRecords vv;
                avro::decode(d, vv);
                v.set_AvroRecords(vv);
            }
            break;
        case 2:
            {
                c::AvroRequest vv;
                avro::decode(d, vv);
                v.set_AvroRequest(vv);
            }
            break;
        case 3:
            {
                c::AvroProcessedData vv;
                avro::decode(d, vv);
                v.set_AvroProcessedData(vv);
            }
            break;
        case 4:
            {
                c::AvroResponse vv;
                avro::decode(d, vv);
                v.set_AvroResponse(vv);
            }
            break;
        }
    }
};

}
#endif
