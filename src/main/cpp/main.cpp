#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>

// I changed the include path according to
// my Fedora Linux distribution
#include <json/json.h>


// Protocol Buffers
#include "messages.pb.h"

#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"
#include "ProtobufResponse.h"
#include "AvroResponse.h"
#include "measurements.capnp.h"

#include <capnp/message.h>
#include <capnp/serialize-packed.h>

//#include <boost/array.hpp>
#include <boost/asio.hpp>

#include "measurements.hh"

#include "CapnProto.h"

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');
    std::cout << s << std::endl;
//    std::cout << s << std::endl;

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){
    int size;
    stream >> size;
    std::unique_ptr<avro::InputStream> in = avro::istreamInputStream(stream, size);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);

    c::AvroRequest avroRequest;
    avro::decode(*d, avroRequest);

    c::AvroResponse avroResponse;
    for (int i = 0; i < avroRequest.records.size(); i++) {
        avroResponse.pd.push_back(c::AvroProcessedData());
        processData(avroResponse.pd[i], avroRequest.records[i]);
    }

    std::unique_ptr<avro::OutputStream> out = avro::ostreamOutputStream(stream);
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*out);
    avro::encode(*e, avroResponse);
    e->flush();

//    throw std::logic_error("TODO: Implement avro");
}

void processProtobuf(tcp::iostream& stream){
    Request request;
    Response response;

    uint32_t size;
    stream >> size;

    char *buffer = new char[size];
    stream.read(buffer, size);

    if (!request.ParseFromArray((void *) buffer, size)) {
        cerr << "Failed to parse request." << endl;
        return;
    }
    delete buffer;

    for (int i = 0; i < request.records_size(); i++) {
        processData(response.add_pd(), request.records(i));
    }

    response.SerializePartialToOstream(&stream);
//    throw std::logic_error("TODO: Implement profobuf");
}

void processCapnproto(tcp::iostream& stream) {
    std::string ok_connection;
    stream >> ok_connection;
    std::cout << ok_connection << std::endl;
    FILE* file = fopen("/tmp/messages.out", "rb");
    int fd = fileno(file);
    ::capnp::ReaderOptions readerOptions;
    readerOptions.traversalLimitInWords = INT64_MAX;
    ::capnp::PackedFdMessageReader messageReader(fd, readerOptions);

    ::capnp::MallocMessageBuilder messageInit;
    CapnpRequest::Reader req = messageReader.getRoot<CapnpRequest>();
    CapnpResponse::Builder response = messageInit.initRoot<CapnpResponse>();
    response.initPd(req.getRecords().size());
    for (int i = 0; i < req.getRecords().size(); i++) {
        CapnpProcessedData::Builder pd = response.getPd()[i];
        processDataCapn(pd, req.getRecords()[i]);
        response.getPd()[i] = pd;
//        std::cout << response.getPd()[i].getAvgDownload() << std::endl;

    }
    fclose(file);
    file = fopen("/tmp/messages.out", "wb");
    fd = fileno(file);
    capnp::writeMessageToFd(fd, messageInit);
    std::cout << messageInit.getRoot<CapnpResponse>().getPd()[0].getAvgDownload() << std::endl;
    stream << "PR   OCITAL";

}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if (protocol == "json") {
                processJSON(stream);
            } else if (protocol == "avro") {
                processAvro(stream);
            } else if (protocol == "proto") {
                GOOGLE_PROTOBUF_VERIFY_VERSION;
                processProtobuf(stream);
            } if (protocol == "capn" ) {
                processCapnproto(stream);
            } else {
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
