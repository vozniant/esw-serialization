//
// Created by anton on 5/3/21.
//

#ifndef APP_CAPNPROTO_H
#define APP_CAPNPROTO_H

#include "measurements.capnp.h"

double calculateAverage(CapnpRecordsMap::Reader data, int j);
void processDataCapn(CapnpProcessedData::Builder &pd, CapnpRecords::Reader record);

#endif //APP_CAPNPROTO_H
