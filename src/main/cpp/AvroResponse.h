//
// Created by anton on 4/29/21.
//

#ifndef APP_AVRORESPONSE_H
#define APP_AVRORESPONSE_H
#include <map>
#include <string>
#include <vector>

typedef std::map<std::string, std::vector<double >> map_data;
typedef std::map<std::string, double> response_data;

#include "measurements.hh"

response_data calculateAverageAvro(map_data data);
void processData(c::AvroProcessedData &avroProcessedData, const c::AvroRecords &records);

#endif //APP_AVRORESPONSE_H
