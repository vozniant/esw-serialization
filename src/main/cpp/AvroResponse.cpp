//
// Created by anton on 4/29/21.
//

#include "AvroResponse.h"


response_data calculateAverageAvro(map_data data) {
    response_data r_data;
    double avg_upload = 0., avg_download = 0., avg_ping = 0.;

    auto records_upload = data["UPLOAD"];
    auto records_download = data["DOWNLOAD"];
    auto records_ping = data["PING"];

    for (int i = 0; i < records_upload.size(); i++) {
        avg_upload = avg_upload + records_upload[i];
    }
    avg_upload /= records_upload.size();

    for (int i = 0; i < records_download.size(); i++) {
        avg_download += records_download[i];
    }
    avg_download /= records_download.size();

    for (int i = 0; i < records_ping.size(); i++) {
        avg_ping += records_ping[i];
    }
    avg_ping /= records_ping.size();

    r_data["DOWNLOAD"] = avg_download;
    r_data["UPLOAD"] = avg_upload;
    r_data["PING"] = avg_ping;

    return r_data;
}

void processData(c::AvroProcessedData &avroProcessedData, const c::AvroRecords &records) {
    avroProcessedData.info = records.info;
    avroProcessedData.avg = calculateAverageAvro(records.records);
}
