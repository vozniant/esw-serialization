//
// Created by anton on 5/3/21.
//

#include "CapnProto.h"

double calculateAverage(CapnpRecordsMap::Reader data, int j) {
    double avg = 0;
    for (int i = 0; i < data.getEntries()[j].getValue().size(); i++) {
        avg += data.getEntries()[j].getValue()[i];
    }

    return avg / data.getEntries().size();
}
void processDataCapn(CapnpProcessedData::Builder &pd, CapnpRecords::Reader record) {
    pd.setAvgDownload(calculateAverage(record.getRecords(), 0));
    pd.setAvgUpload(calculateAverage(record.getRecords(), 1));
    pd.setAvgPing(calculateAverage(record.getRecords(), 2));

    pd.setInfo(record.getInfo());
}
