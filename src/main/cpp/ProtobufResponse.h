//
// Created by anton on 4/26/21.
//

#ifndef APP_PROTOBUFRESPONSE_H
#define APP_PROTOBUFRESPONSE_H

#include "messages.pb.h"

void processData(ProcessedData *pd, const Records &records);
Averages *calculateAverage(const std::map<std::string, std::vector<double > > records);

#endif //APP_PROTOBUFRESPONSE_H
